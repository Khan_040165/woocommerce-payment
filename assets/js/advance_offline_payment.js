jQuery("#advance_file").on('change', function () {
	if (!this.files.length) {
		jQuery("#advance_file_lists").empty();
	} else {
		const file = this.files[0];
		const doc_name = file.name;
		const doc_ext = doc_name.split('.').pop().toLowerCase();
		if (jQuery.inArray(doc_ext, ['doc', 'docx', 'pdf', 'mpdf', '']) == -1) {
			alert('invalid file extension');
		} else {
			jQuery("#advance_file_lists").html('<a href="' + URL.createObjectURL(file) + '">' + doc_name + ' </a>' + '<span id="delete" style="margin-left:10px;"> ' + '&times;' + '</span>');
			jQuery("label[for='advance_file'], input#advance_file").hide();
		}
		const formData = new FormData();
		//const wcObj = 'wc_checkout_params';
		formData.append('advance_file', file);
		//formData.append('action', action);
		formData.append('nonce', wcObj.nonce);
		jQuery.ajax({
			url: wc_checkout_params.ajax_url + '?action=file_upload',
			type:'POST',
			data: formData,
			contentType: false,
			enctype: 'multipart/form-data',
			//dataType: 'json',
			processData: false,
			success: function (response) {
				//alert(response);
				//console.log(response);
				jQuery('input[name="advance_hidden_field"]').val(response);
			}
			// error: function(response) {
			// 	console.log(response);
			// }
		});
	}
	return false;
});
