<?php
/**
 * Plugin Name: Advanced Bank Payment Transfer
 * Plugin URI: http://wp.development/woo-direct-bank-payment-transfer/
 * Plugin Author: Ridwanur Khan
 * Author: Ridwanur Khan
 * Author URI: https://github.com/Ridwanur786
 * Description: A payment System That Process Only Local Payment
 * Version:1.0.1
 * License: GNU General Public License v3.0
 * License URI: http://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain: ad-bank-payment-system
 */

defined('ABSPATH') or exit;

require_once(dirname(__FILE__) . '/../../../wp-load.php');

if (!in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {
	return;
}

add_action('plugins_loaded', 'ad_bank_payment_init', 11);
//var_dump(plugins_url('fileUpload.js',__FILE__));


function ad_gateway_plugin_links($links)
{

	$plugin_links = array(
		'<a href="' . admin_url('admin.php?page=wc-settings&tab=checkout') . '">' . __('Configure', 'ad-bank-payment-system') . '</a>'
	);

	return array_merge($plugin_links, $links);
}


add_filter('plugin_action_links_' . plugin_basename(__FILE__), 'ad_gateway_plugin_links');

function add_to_advance_payment_gateway($methods)
{
	$methods[] = 'WC_Gateway_Advance_Bank_Payment_Transfer_Gateway';
	return $methods;
}
add_filter('woocommerce_payment_gateways', 'add_to_advance_payment_gateway');

function ad_bank_payment_init()
{
	if (!class_exists('WC_Gateway_Advance_Bank_Payment_Transfer_Gateway')) {
		class WC_Gateway_Advance_Bank_Payment_Transfer_Gateway extends WC_Payment_Gateway
		{
			public $localization;

			public function __construct()
			{
				$this->id                 = 'advance_offline_payment';
				$this->icon               = apply_filters('woo_pay_icon', plugins_url('/assets/icon.png', __FILE__));
				$this->has_fields         = true;
				$this->method_title       = __('Advanced Bank Payment Transfer', 'ad-bank-payment-system');
				$this->method_description = __('A payment System That Process Only Local Payment', 'ad-bank-payment-system');
				$this->init_form_fields();
				$this->init_settings();

				// Define user set variables
				$this->title        = $this->get_option('title');
				$this->description  = $this->get_option('description');

				// $this->advance_file =  $this->get_option('advance_file');
				$this->instructions = $this->get_option('instructions', $this->description);
				$this->account_details = get_option(
					'advance_bank_account_details',
					array(
						array(
							'account_name'     => $this->get_option('account_name'),
							'account_number'     => $this->get_option('account_number'),
							'sort_code'     => $this->get_option('sort_code'),
							'iban'     => $this->get_option('iban'),
							'bic'     => $this->get_option('bic'),
							'bank_name'     => $this->get_option('bank_name')
						)
					)
				);

				// Actions
				add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
				add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'save_advance_account_details'));
				add_action('woocommerce_thankyou_' . $this->id, array($this, 'thankyou_page'));
				add_filter('woocommerce_gateway_description', [$this, 'advance_upload_bank_receipt'], 20, 2);
				
				add_action('wp_enqueue_scripts',  [$this, 'advance_file_scripts'], 10);
				add_action('woocommerce_checkout_process', [$this, 'custom_file_field_validation']);
				add_action('wp_ajax_file_upload', [$this, 'advance_file_upload']);
				add_action('wp_ajax_nopriv_file_upload', [$this, 'advance_file_upload']);
				add_action('woocommerce_checkout_update_order_meta', [$this, 'save_file_to_the_server'], 10, 1);
				add_action('woocommerce_admin_order_data_after_billing_address', [$this, 'advance_order_meta_general'],10,1);
				add_filter('woocommerce_form_field_file', [$this, 'file_wc_checkout_custom_field'], 10, 4);
				add_action('woocommerce_checkout_create_order', [$this, 'save_file_to_order_meta'], 10, 2);
				add_action('woocommerce_get_order_item_totals', [$this,'display_file_value_in_order_totals'], 10, 4);

				//do_action('woocommerce_gateway_description', [$this, 'advance_file_upload'], 10,3);
				// Customer Emails
				add_action('woocommerce_email_before_order_table', array($this, 'email_instructions'), 10, 3);
				
			}

			function admin_options()
			{
?>
				<h2><?php _e('Advanced Bank Payment Transfer', 'ad-bank-payment-system'); ?></h2>
				<table class="form-table">
					<?php $this->generate_settings_html(); ?>
				</table>

			<?php
			}

			public function init_form_fields()
			{

				$this->form_fields = apply_filters(
					'advance_payment_form_fields',
					array(
						'enabled' => array(
							'title' => __('Enable/Disable', 'ad-bank-payment-system'),
							'type' => 'checkbox',
							'label' => __('Enable Advance Direct Bank Payment', 'ad-bank-payment-system'),
							'default' => 'no',
							'desc_tip'    => true
						),

						'title' => array(
							'title' => __('Title', 'ad-bank-payment-system'),
							'type' => 'text',
							'lable' => __('Enable Advance Direct Bank Transfer', 'ad-bank-payment-system'),
							'description' => __('A payment System That Process Only Local Payment', 'ad-bank-payment-system'),
							'default' => __('A payment System That Process Only Local Payment', 'ad-bank-payment-system'),
							'desc_tip' => true
						),

						'description' => array(
							'title' => __('Description', 'ad-bank-payment-system'),
							'type' => 'textarea',
							'description' => __('Make Your Payment Directly Into Our Bank Account.', 'ad-bank-payment-system'),
							'default'     => __('Please remit payment to Store Name upon pickup or delivery.', 'ad-bank-payment-system'),
							'desc_tip'    => true,
						),

						'instructions' => array(
							'title'       => __('Instructions', 'ad-bank-payment-system'),
							'type'        => 'textarea',
							'description' => __('Instructions that will be added to the thank you page and emails.', 'ad-bank-payment-system'),
							'default'     => '',
							'desc_tip'    => true
						),

						'account_details' => array(
							'type' => 'account_details'
						)
					)
				);
			}


			public function advance_file_scripts()
			{
				// global $wp;
				// if (is_checkout() && empty($wp->query_vars['order-pay']) && !isset($wp->query_vars['order-received'])) {
				wp_enqueue_script('advance_offline_payment', plugin_dir_url( __FILE__).'assets/js/advance_offline_payment.js', array('jquery'), true);
				$nonce = wp_create_nonce('nonce_file');

				wp_enqueue_style('custom', plugins_url('/assets/plugin_css/custom.css', __FILE__));
				wp_localize_script(
					'advance_offline_payment',
					'wc_checkout_params',
					array(
						'ajax_url' => admin_url('admin-ajax.php'),
						'nonce' => $nonce
					)
				);
				//wp_deregister_script('wc-checkout');
				//wp_enqueue_script('wc-checkout', plugins_url('/woocommerce/js/checkout.js', __FILE__), array('jquery', 'woocommerce', 'wc-country-select', 'wc-address-i18n'), null, true); 
				
				// }

				//wp_enqueue_script( 'fileUpload' );
			}

			public function file_wc_checkout_custom_field($field, $key, $args, $value)
			{

				$sort              = $args['priority'] ? $args['priority'] : '';
				$container_class   = esc_attr(implode(' ', $args['class']));
				$input_class       = esc_html(implode('', $args['input_class']));
				$container_id      = esc_attr($args['id']) . '_field';
				$custom_attributes         = array();

				$field = '<input type="' . wp_kses_post($args['type']) . '" class=" ' . $input_class . '" value="' . esc_attr($value) . '"  placeholder="' . esc_attr($args['placeholder']) . '" name="' . esc_attr($key) . '" id="' . esc_attr($args['id']) . '" ' . implode(' ', $custom_attributes) . '/>';

				if ($args['type'] === 'hidden') {
					$filed .= '<input type="' . wp_kses_post($args['type']) . '" class="' .  $input_class . '"' . esc_attr(implode('', $args['input_class'])) . '"value="' . esc_attr($value) . '"name="' . esc_attr($key) . '"id="' . esc_attr($args['id']) . '" ' . implode('', $custom_attributes) . '/>';
				}

				$field .= '<label for="' . wp_kses_post($args['id']) . '">' . '<a>' . wp_kses_post($args['label']) . '</a>' . '</label>';
				$field .= '<div id="advance_file_lists"></div>';
				ob_start();
			?>
				<p class="form-row <?php echo $container_class; ?>" id="<?php echo $container_id; ?>" data-priority="<?php echo esc_attr($sort); ?>">

					<?php echo $field; ?>
				</p>
				<?php return ob_get_clean(); ?>


				<?php
			}
			public function advance_upload_bank_receipt($description, $advance_payment_id)
			{
				if ('advance_offline_payment' === $advance_payment_id) {
					ob_start();
					woocommerce_form_field('advance_file', array(
						'type' => 'file',
						'label' => __('Upload Bank Receipt', 'ad-bank-payment-system'),
						'id'    => 'advance_file',
						'class' => array('file_form_field form-row-wide'),
						'input_class' => array('advance_custom_field'),
						'required' => true

					), '');
					woocommerce_form_field('advance_hidden_field', array(
						'type' => 'hidden',
						'class' => array('form-row-wide'),
						'input_class' => array('advance_hidden_field'),
						'id'   => 'advance_hidden_field'

					), ''); ?>
			
				<?php
					$description .= ob_get_clean(); // Append  buffered content
				}

				return $description;
			}


			public function custom_file_field_validation(){
					if ('advance_offline_payment' === isset($_POST['payment_method'])  && !isset($_POST['advance_file']) || empty($_POST['advance_file'])) {
					// var_dump($_FILES);
					// die();

					//$passed = false;
					wc_add_notice(__('upload a document for "Direct Bank Transfer" payment, please.'), 'error');
				}

				return $passed;
			}

			public function save_file_to_order_meta($order, $data){
						if(isset($_POST['advance_hidden_field']) && !empty($_POST['advance_hidden_field'])){
							$order->update_meta_data('_advance_hidden_field', sanitize_text_field($_POST['advance_hidden_field']));
						}
			}

			public function advance_file_upload()
			{

				// var_dump($_FILES);
				// exit();
				session_start();
				//var_dump($_FILES);
				//die();
				if (!wp_verify_nonce($_POST['nonce'], 'nonce_file'))
					wp_die();

				if (!function_exists('wp_handle_upload')) {

					if (isset($_FILES['advance_file']) && empty($_FILES['advance_file'])) {
						//var_dump($_FILES);
						//die();
					
						
							require_once('wp-admin/includes/file.php');
							$upload_dir = wp_upload_dir();
							$path = $upload_dir['path'] . '/' . basename($_FILES['advance_file']['name']);

							if (move_uploaded_file($_FILES['advance_file']['tmp_name'], $path)) {
								echo $upload_dir['url'] . '/' . basename($_FILES['advance_file']['name']);
							}
						}
					}
				
				wp_die();
			}


			public function display_file_value_in_order_totals($total_rows, $order, $file_options){
				if($order->get_payment_method() === 'advance_offline_payment' && $file_options = $order->get_meta('_advance_hidden_field')){
					$sorted_total_files = [];

					foreach($total_rows as $key => $total_Row){
						if($key == 'payment_method'){
							$sorted_total_files['advance_file'] =[
								'label' => __('attached Document', 'ad-bank-payment-system'),
								'value' => esc_html($file_options),
							];
						}
					}
					$total_rows = $sorted_total_files;
				}
				return $total_rows;
			}
			public function save_file_to_the_server($order_id)
			{

				if (!empty($_POST['advance_hidden_field'])) {
					update_post_meta($order_id, 'advance_hidden_field', sanitize_text_field($_POST['advance_hidden_field']));
				}
				// var_dump(update_post_meta());
				// die();
			}
			// //var_dump( advance_file_upload());
			function advance_order_meta_general($order)
			{

				if($file_options = $order->get_meta('_advance_hidden_field')){

				
				// $file = get_post_meta($order->get_id(), 'advance_hidden_field', true);
				// if ($file) {
					echo '<img src="' . esc_url($file_options) . '" >';
				}
			}
			public function generate_account_details_html()
			{

				ob_start();

				$country = WC()->countries->get_base_country();
				$localization  = $this->get_country_locale();

				// Get sortcode label in the $localization array and use appropriate one.
				$advanceSortCode = isset($localization[$country]['sortcode']['label']) ? $localization[$country]['sortcode']['label'] : __('Sort code', 'ad-bank-payment-system');
				?>

				<tr valign="top">
					<th scope="row" class="titledesc"><?php esc_html_e('Account details:', 'ad-bank-payment-system'); ?></th>
					<td class="forminp" id="advance_accounts">
						<div class="wc_input_table_wrapper">
							<table class="widefat wc_input_table sortable" cellspacing="0">
								<thead>
									<tr>
										<th class="sort">&nbsp;</th>
										<th><?php esc_html_e('Account name', 'ad-bank-payment-system'); ?></th>
										<th><?php esc_html_e('Account number', 'ad-bank-payment-system'); ?></th>
										<th><?php esc_html_e('Bank name', 'ad-bank-payment-system'); ?></th>
										<th><?php echo esc_html($advanceSortCode); ?></th>
										<th><?php esc_html_e('IBAN', 'ad-bank-payment-system'); ?></th>
										<th><?php esc_html_e('BIC / Swift', 'ad-bank-payment-system'); ?></th>
									</tr>
								</thead>
								<tbody class="accounts">
									<?php
									$i = -1;
									if ($this->account_details) {
										foreach ($this->account_details as $advanceAccount) {
											$i++;

											echo '<tr class="account">
										<td class="sort"></td>
										<td><input type="text" value="' . esc_attr(wp_unslash($advanceAccount['account_name'])) . '" name="advance_account_name[' . esc_attr($i) . ']" /></td>
										<td><input type="text" value="' . esc_attr($advanceAccount['account_number']) . '" name="advance_account_number[' . esc_attr($i) . ']" /></td>
										<td><input type="text" value="' . esc_attr(wp_unslash($advanceAccount['bank_name'])) . '" name="advance_bank_name[' . esc_attr($i) . ']" /></td>
										<td><input type="text" value="' . esc_attr($advanceAccount['sort_code']) . '" name="advance_sort_code[' . esc_attr($i) . ']" /></td>
										<td><input type="text" value="' . esc_attr($advanceAccount['iban']) . '" name="advance_iban[' . esc_attr($i) . ']" /></td>
										<td><input type="text" value="' . esc_attr($advanceAccount['bic']) . '" name="advance_bic[' . esc_attr($i) . ']" /></td>
									</tr>';
										}
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<th colspan="7"><a href="#" class="add button"><?php esc_html_e('+ Add account', 'ad-bank-payment-system'); ?></a> <a href="#" class="remove_rows button"><?php esc_html_e('Remove selected account(s)', 'ad-bank-payment-system'); ?></a></th>
									</tr>
								</tfoot>
							</table>
						</div>
						<script type="text/javascript">
							jQuery(function() {
								jQuery('#advance_accounts').on('click', 'a.add', function() {

									var size = jQuery('#advance_accounts').find('tbody .account').length;

									jQuery('<tr class="account">\
									<td class="sort"></td>\
									<td><input type="text" name="advance_account_name[' + size + ']" /></td>\
									<td><input type="text" name="advance_account_number[' + size + ']" /></td>\
									<td><input type="text" name="advance_bank_name[' + size + ']" /></td>\
									<td><input type="text" name="advance_sort_code[' + size + ']" /></td>\
									<td><input type="text" name="advance_iban[' + size + ']" /></td>\
									<td><input type="text" name="advance_bic[' + size + ']" /></td>\
								</tr>').appendTo('#advance_accounts table tbody');

									return false;
								});
							});
						</script>
					</td>
				</tr>
<?php
				return ob_get_clean();
			}

			public function save_advance_account_details()
			{

				$advanceAccount = array();

				// phpcs:disable WordPress.Security.NonceVerification.Missing -- Nonce verification already handled in WC_Admin_Settings::save()
				if (
					isset($_POST['advance_account_name']) && isset($_POST['advance_account_number']) && isset($_POST['advance_bank_name'])
					&& isset($_POST['advance_sort_code']) && isset($_POST['advance_iban']) && isset($_POST['advance_bic'])
				) {

					$account_names   = wc_clean(wp_unslash($_POST['advance_account_name']));
					$account_numbers = wc_clean(wp_unslash($_POST['advance_account_number']));
					$bank_names      = wc_clean(wp_unslash($_POST['advance_bank_name']));
					$sort_codes      = wc_clean(wp_unslash($_POST['advance_sort_code']));
					$ibans           = wc_clean(wp_unslash($_POST['advance_iban']));
					$bics            = wc_clean(wp_unslash($_POST['advance_bic']));

					foreach ($account_names as $i => $name) {
						if (!isset($account_names[$i])) {
							continue;
						}

						$advanceAccount[] = array(
							'account_name'   => $account_names[$i],
							'account_number' => $account_numbers[$i],
							'bank_name'      => $bank_names[$i],
							'sort_code'      => $sort_codes[$i],
							'iban'           => $ibans[$i],
							'bic'            => $bics[$i],
						);
					}
				}
				// phpcs:enable

				do_action('woocommerce_update_option', array('id' => 'advance_bank_account_details'));
				update_option('advance_bank_account_details', $advanceAccount);
			}

			public function thankyou_page($order_id)
			{
				if ($this->instructions) {
					echo wp_kses_post(wpautop(wptexturize(wp_kses_post($this->instructions))));
				}

				return $this->advance_bank_details($order_id);
			}


			public function email_instructions($order, $sent_to_admin, $plain_text = false)
			{

				if ($this->instructions && !$sent_to_admin && $this->id === $order->payment_method && $order->has_status('on-hold')) {
					echo wpautop(wptexturize($this->instructions)) . PHP_EOL;
				}

				$this->advance_bank_details($order->get_id());
			}


			private function advance_bank_details($order_id = '')
			{

				if (empty($this->account_details)) {
					return;
				}

				// Get order and store in $order.
				$order = wc_get_order($order_id);

				// Get the order country and country $localization.
				$country = $order->get_billing_country();
				$localization  = $this->get_country_locale();

				// Get sortcode label in the $localization array and use appropriate one.
				$sortcode = isset($localization[$country]['sortcode']['label']) ? $localization[$country]['sortcode']['label'] : __('Sort code', 'woocommerce');

				$advance_accounts = apply_filters('advance_bank_account_details', $this->account_details, $order_id);

				if (!empty($advance_accounts)) {
					$advance_account_html = '';
					$has_details  = false;

					foreach ($advance_accounts as $advance_account) {
						$advance_account = (object) $advance_account;

						if ($advance_account->account_name) {
							$advance_account_html .= '<h3 class="wc-bacs-bank-details-account-name">' . wp_kses_post(wp_unslash($advance_account->account_name)) . ':</h3>' . PHP_EOL;
						}

						$advance_account_html .= '<ul class="wc-bacs-bank-details order_details bacs_details">' . PHP_EOL;

						// BACS account fields shown on the thanks page and in emails.
						$advance_account_fields = apply_filters(
							'advance_account_fields',
							array(
								'bank_name'      => array(
									'label' => __('Bank', 'ad-bank-payment-system'),
									'value' => $advance_account->bank_name,
								),
								'account_number' => array(
									'label' => __('Account number', 'ad-bank-payment-system'),
									'value' => $advance_account->account_number,
								),
								'sort_code'      => array(
									'label' => $sortcode,
									'value' => $advance_account->sort_code,
								),
								'iban'           => array(
									'label' => __('IBAN', 'ad-bank-payment-system'),
									'value' => $advance_account->iban,
								),
								'bic'            => array(
									'label' => __('BIC', 'ad-bank-payment-system'),
									'value' => $advance_account->bic,
								),
							),
							$order_id
						);

						foreach ($advance_account_fields as $field_key => $field) {
							if (!empty($field['value'])) {
								$advance_account_html .= '<li class="' . esc_attr($field_key) . '">' . wp_kses_post($field['label']) . ': <strong>' . wp_kses_post(wptexturize($field['value'])) . '</strong></li>' . PHP_EOL;
								$has_details   = true;
							}
						}

						$advance_account_html .= '</ul>';
					}

					if ($has_details) {
						echo '<section class="woocommerce-bacs-bank-details"><h2 class="wc-bacs-bank-details-heading">' . esc_html__('Our bank details', 'ad-bank-payment-system') . '</h2>' . wp_kses_post(PHP_EOL . $advance_account_html) . '</section>';
					}
				}
			}

			public function process_payment($order_id)
			{

				$order = wc_get_order($order_id);

				if ($order->get_total() > 0) {
					// Mark as on-hold (we're awaiting the payment).
					$order->update_status(apply_filters('woocommerce_advance_offline_payment_order_status', 'on-hold', $order), __('Awaiting Advance payment', 'ad-bank-payment-system'));
				} else {
					$order->payment_complete();
				}

				// Remove cart.
				WC()->cart->empty_cart();

				// Return thankyou redirect.
				return array(
					'result'   => 'success',
					'redirect' => $this->get_return_url($order),
				);
			}


			public function get_country_locale()
			{

				if (empty($this->localization)) {

					// Locale information to be used - only those that are not 'Sort Code'.
					$this->localization = apply_filters(
						'advance_get_banks_locale',
						array(
							'AU' => array(
								'sortcode' => array(
									'label' => __('BSB', 'ad-bank-payment-system'),
								),
							),
							'CA' => array(
								'sortcode' => array(
									'label' => __('Bank transit number', 'ad-bank-payment-system'),
								),
							),
							'IN' => array(
								'sortcode' => array(
									'label' => __('IFSC', 'ad-bank-payment-system'),
								),
							),
							'IT' => array(
								'sortcode' => array(
									'label' => __('Branch sort', 'ad-bank-payment-system'),
								),
							),
							'NZ' => array(
								'sortcode' => array(
									'label' => __('Bank code', 'ad-bank-payment-system'),
								),
							),
							'SE' => array(
								'sortcode' => array(
									'label' => __('Bank code', 'ad-bank-payment-system'),
								),
							),
							'US' => array(
								'sortcode' => array(
									'label' => __('Routing number', 'ad-bank-payment-system'),
								),
							),
							'ZA' => array(
								'sortcode' => array(
									'label' => __('Branch code', 'ad-bank-payment-system'),
								),
							),
						)
					);
				}

				return $this->localization;
			}
		}
	}
}
